<?php

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function waitUserUpdateData()
{
    sleep(rand(1, 10));

    $data = [
        'id' => rand(1, 10),
        'name' => generateRandomString(rand(5, 20)),
        'gender' => rand(0, 1) == 0 ? 'male' : 'female',
    ];

    return $data;
}

// Ждем, когда пользователь изменит данные и выводим эти данные в терминал
while($data = waitUserUpdateData()) {
    print_r($data);
}

